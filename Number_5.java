import java.util.Scanner;

public class Number_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter budget: ");
        int budget = scanner.nextInt();
        System.out.println("Enter summer or winter: ");
        String season = scanner.next();

        String destination = "Bulgaria";
        String vacationType = "Camp";
        double spentAmount = 0;

        if (budget <= 100) {
            destination = "Bulgaria";
            if (season.equals("summer")) {
                vacationType = "Camp";
                spentAmount = budget * 0.3;
            } else if (season.equals("winter")) {
                vacationType = "Hotel";
                spentAmount = budget * 0.7;
            }
        } else if (budget <= 1000) {
            destination = "Balkans";
            if (season.equals("summer")) {
                vacationType = "Camp";
                spentAmount = budget * 0.4;
            } else if (season.equals("winter")) {
                vacationType = "Hotel";
                spentAmount = budget * 0.8;
            }
        } else {
            destination = "Europe";
            vacationType = "Hotel";
            spentAmount = budget * 0.9;
        }


        System.out.printf("Somewhere in %s%n", destination);
        System.out.printf("%s - %.2f", vacationType, spentAmount);

    }
}
