import java.util.Scanner;

public class Number_7 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter month: ");
        String month = scan.nextLine();
        System.out.println("Enter number of days you stay: ");
        int days = Integer.parseInt(scan.nextLine());
        System.out.println("Enter type of accomodation (studio or apartment): ");
        String accomodation = scan.nextLine();

        double final_sum = 0;
        double discount = 0;

        if (accomodation.equals("studio")) {
            if (month.equals("May") || month.equals("October")) {
                if (days > 7) {
                    discount = 0.05;
                    final_sum = (days * 50) * discount;
                } else if (days > 14) {
                    discount = 0.3;
                    final_sum = (days * 50) * discount;
                }
            } else if (month.equals("June") || month.equals("September")) {
                if (days > 14) {
                    discount = 0.2;
                    final_sum = (days * 75.20) * discount;
                }
            }

        } else if (accomodation.equals("apartment")) {
            if (days > 14) {
                discount = 0.1;
            }
        }
        System.out.println(final_sum);
    }
}