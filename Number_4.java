import java.util.Scanner;

public class Number_4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your budget: ");
        int budget = Integer.parseInt(scan.nextLine());
        System.out.println("Enter season: ");
        String season = scan.nextLine();
        System.out.println("Enter number of fishermen: ");
        int fishermenCount = Integer.parseInt(scan.nextLine());

        double boatRent = 0;

        if (season.equals("Spring")) {
            boatRent = 3000;
        } else if (season.equals("Summer") || season.equals("Autumn")) {
            boatRent = 4200;
        } else if (season.equals("Winter")) {
            boatRent = 2600;
        }

        double discount = 0;
        if (fishermenCount <= 6) {
            discount = 0.1;
        } else if (fishermenCount <= 11) {
            discount = 0.15;
        } else {
            discount = 0.25;
        }

        if (budget >= boatRent) {
            System.out.printf("Yes! You have %.2f leva left.", budget - boatRent);
        } else {
            System.out.printf("Not enough money! You need %.2f leva.", boatRent - budget);
        }
    }
}

