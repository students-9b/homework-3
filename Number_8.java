import java.util.Scanner;

public class Number_8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the hour of the exam: ");
        double exam_hour = Double.parseDouble(scan.nextLine());
        System.out.println("Enter the minutes of the exam: ");
        double exam_mins = Double.parseDouble(scan.nextLine());
        System.out.println("Enter the hour of arriving: ");
        double arr_hour = Double.parseDouble(scan.nextLine());
        System.out.println("Enter the minutes of arriving");
        double arr_mins = Double.parseDouble(scan.nextLine());

        if (arr_hour > exam_hour || arr_mins > exam_mins){
            System.out.println("Late");
        }else if (arr_hour <= exam_hour && arr_mins >= (exam_mins-30)){
            System.out.println("On time");
        }else if (arr_mins < (exam_mins-30) && arr_hour < exam_hour){
            System.out.println("Early");
        }



    }
}

