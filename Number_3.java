import java.util.Scanner;

public class Number_3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter  'Roses', 'Dahlias', 'Tulips', 'Narcissus' or 'Gladiolus; ");
        String flower = scan.nextLine();
        System.out.println("enter number of flowers you'd like: ");
        int number = Integer.parseInt(scan.nextLine());
        System.out.println("Enter your budget: ");
        int budget = Integer.parseInt(scan.nextLine());
        double Roses = number * 5;
        double Dahlias = number * 3.80;
        double Tulips = number * 2.80;
        double Narcissus = number * 3;
        double Gladiolus = number * 2.50;

        double final_sum = Roses + Dahlias + Tulips + Narcissus + Gladiolus;


        if (number > 80 && flower.equals("Roses")){
            final_sum = final_sum - (final_sum/0.10);
        }else if (number > 90 && flower.equals("Dahlias")){
            final_sum = final_sum - (final_sum/0.15);
        }else if (number > 80 && flower.equals("Tulips")){
            final_sum = final_sum - (final_sum/0.15);
        }else if (number < 120 && flower.equals("Narcissus")){
            final_sum = final_sum + (final_sum/0.15);
        }else if (number < 80 && flower.equals("Gladiolus")){
            final_sum = final_sum + (final_sum/0.20);
        }

        double left = budget - final_sum;
        double more = final_sum - budget;

        if (budget > final_sum){
            System.out.printf("You have a great garden with " + number + flower + " and " + left + " leva left.");
        }else if (budget < final_sum){
            System.out.printf("Not enough money, you need " + more + " leva more.");
        }


    }
}
