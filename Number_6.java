import java.util.Scanner;

public class Number_6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first number: ");
        int num1 = Integer.parseInt(scan.nextLine());
        System.out.println("Enter the second number: ");
        int num2 = Integer.parseInt(scan.nextLine());
        System.out.println("Enter operation: ");
        String operation = scan.nextLine();
        double result = 0;

        if (operation.equals("+")) {
            result = num1 + num2;
            System.out.printf("%d + %d = %s", num1, num2, result);
        } else if (operation.equals("-")) {
            result = num1 - num2;
            System.out.printf("%d - %d = %s", num1, num2, result);
        } else if (operation.equals("*")) {
            result = num1 * num2;
            System.out.printf("%d * %d = %s", num1, num2, result);
        } else if (operation.equals("/")) {
            result = num1 / num2;
            System.out.printf("%d / %d = %.2fs", num1, num2, result);
            if (num2 == 0) {
                System.out.println("You can't divide by zero!");
            }
        }

    }

}
